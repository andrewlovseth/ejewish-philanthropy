		<footer>
			<div class="wrapper">

				<?php get_template_part('template-parts/footer/logo'); ?>

				<div class="footer-utilities">
					<?php get_template_part('template-parts/footer/navigation'); ?>

					<?php get_template_part('template-parts/footer/social'); ?>

					<?php get_template_part('template-parts/footer/subscribe'); ?>
				</div>

				<?php get_template_part('template-parts/footer/copyright'); ?>

			</div>
		</footer>

	</div> <!-- #page -->

	<?php get_template_part('template-parts/global/search-form'); ?>

	<?php get_template_part('template-parts/global/subscribe-overlay'); ?>

	<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='birthday';fnames[4]='MMERGE4';ftypes[4]='zip';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
	
	<?php wp_footer(); ?>

	<?php the_field('footer_javascript', 'options'); ?>

</body>
</html>