<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section class="main two-col-content">
		<?php get_template_part('template-parts/home/sidebar'); ?>

		<section class="content">
			<div class="content-wrapper">
				<?php get_template_part('template-parts/home/featured-articles'); ?>

				<?php get_template_part('template-parts/global/ads/ad-1'); ?>

				<?php get_template_part('template-parts/home/subscribe'); ?>			
			</div>
		</section>
	</section>

<?php get_footer(); ?>