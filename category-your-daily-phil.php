<?php get_header(); ?>

    <section class="archive-page-header">
		<div class="wrapper">

			<div class="headline">
				<h1 class="x-large-title"><?php single_cat_title(); ?></h1>
			</div>
			
		</div>
    </section>	
    	
    <main class="content grid-archive">
        <div class="wrapper">
        
            <?php get_template_part('template-parts/global/grid-archive'); ?>

        </div>
    </main>
	
<?php get_footer(); ?>