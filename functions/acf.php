<?php

// Sort relationship fields by date
function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);


// Add Site Options page
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Site Options');
}

// Style relationship fields
function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>
	<?php
}
add_action('acf/input/admin_head', 'my_acf_admin_head');