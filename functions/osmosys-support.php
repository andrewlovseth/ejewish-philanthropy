<?php

add_filter('render_block', 'bearsmith_custom_image_block', 10, 2 );

function bearsmith_custom_image_block( $block_content, $block ) {

    if ( 'core/image' !== $block['blockName'] ) {
        return $block_content;
    }

    $blockHTML = $block['innerHTML'];
    $new = array('<p class="figcaption">', '</p>');
    $original = array('<figcaption>', '</figcaption>');

    $modifiedHTML = str_replace($original, $new, $blockHTML);

    return $modifiedHTML;
}