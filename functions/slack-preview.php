<?php

function bearsmith_slack_enhanced_preview( $data, $presentation ) {
    $data = [];
    $post_content = $presentation->source->post_content;
    $authors = get_field('authors', $presentation->model->object_id);

    if ( is_singular( 'post' ) ) {
        $author_display = '';
        
        if( $authors ) {
            foreach($authors as $author) {
                $author_display .= get_the_title( $author->ID ) . ', ';
            }

            $author_display = rtrim($author_display, ', ');
        }

        if(count($authors) == 1) {
            $author_label = 'Author';
        } else {
            $author_label = 'Authors';
        }

        if ( ! empty( $author_display ) ) {
            $data[ __( $author_label, 'wordpress-seo' ) ] = $author_display;
        }

		if ( ! empty( $post_content ) ) {
			$data[ __( 'Est. reading time', 'wordpress-seo' ) ] = get_reading_time( $post_content );
		}
    }
  
    return $data;
  }
  add_filter( 'wpseo_enhanced_slack_data', 'bearsmith_slack_enhanced_preview', 10, 2 );


  function get_reading_time( $post_content ) {
    // 250 is the estimated words per minute, https://en.wikipedia.org/wiki/Speed_reading.
    $words_per_minute = 250;

    $word    = \str_word_count( \wp_strip_all_tags( $post_content ) );
    $minutes = \round( $word / $words_per_minute );
    /* translators: %s: Time duration in minute or minutes. */
    return \sprintf( \_n( '%s minute', '%s minutes', $minutes, 'default' ), $minutes );
}