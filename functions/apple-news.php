<?php


// Add Featured Image to Apple News
function add_post_meta_content($content, $post_id) {

    $featured_image = get_post_meta( $post_id, 'featured_image', true );
    $meta = wp_get_attachment_image( $featured_image, 'large' );

    return $meta . $content;
}
add_filter('apple_news_exporter_content_pre', 'add_post_meta_content', 10, 2);


// Add Featured Image to RSS
function featured_image_in_feeds( $content ) {
    global $post;

    if( get_field('featured_image', $post->ID ) ) {
        $featured_image = get_post_meta( $post->ID , 'featured_image', true );
        $content = '<p>' . wp_get_attachment_image( $featured_image, 'large', "", array( "class" => "type:primaryImage" ) ) . '</p>' . $content;
    }
    return $content;
}
add_filter( 'the_excerpt_rss', 'featured_image_in_feeds' );
add_filter( 'the_content_feed', 'featured_image_in_feeds' );