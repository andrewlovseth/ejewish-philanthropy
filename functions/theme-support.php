<?php

/*
	Theme Support
*/


// Remove Admin bar from front-end
show_admin_bar(false);


// Theme Support for title tags, post thumbnails, HTML5 elements, feed links
add_theme_support('title-tag');


//Enable support for Post Thumbnails on posts and pages.
add_theme_support('post-thumbnails');



// Switch default core markup for search form, comment form, and comments to output valid HTML5.
add_theme_support('html5', array(
    'comment-list',
    'comment-form',
    'search-form',
    'gallery',
    'caption'
));


// Add default posts and comments RSS feed links to head.
add_theme_support( 'automatic-feed-links' );


// Add support for core custom logo.
add_theme_support('custom-logo', array(
    'height'      => 250,
    'width'       => 250,
    'flex-width'  => true,
    'flex-height' => true,
));


// Add wp_body_open
if ( ! function_exists( 'wp_body_open' ) ) {
    function wp_body_open() {
        do_action( 'wp_body_open' );
    }
}


// Add SVG Support
function bearsmith_add_svg_support($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'bearsmith_add_svg_support');



function bearsmith_excerpt_more( $more ) {
    return '... <a href="'.get_the_permalink().'">Read More</a>';
}
add_filter( 'excerpt_more', 'bearsmith_excerpt_more' );




function bearsmith_disable_author_page() {
    global $wp_query;

    if ( is_author() ) {
        $user = get_queried_object();
        $user_url =  get_author_posts_url($user->ID);
        $user_url_modified = str_replace('/author/', '/authors/', $user_url);

        wp_redirect(site_url($user_url_modified), 301); 
        exit; 
    }
}
add_action('template_redirect', 'bearsmith_disable_author_page');




add_filter( 'yoast_seo_development_mode', '__return_true' );


// functions.php
add_filter( 'wpseo_schema_graph_pieces', 'remove_breadcrumbs_from_schema', 11, 2 );

/**
 * Removes the breadcrumb graph pieces from the schema collector.
 *
 * @param array  $pieces  The current graph pieces.
 * @param string $context The current context.
 *
 * @return array The remaining graph pieces.
 */
function remove_breadcrumbs_from_schema( $pieces, $context ) {
    return \array_filter( $pieces, function( $piece ) {
        return ! $piece instanceof \Yoast\WP\SEO\Generators\Schema\Person;
    } );
}