<?php 

$term = get_queried_object();

get_header(); ?>

	<section class="main two-col-content">

		<section class="content">
			<div class="content-wrapper">			

				<?php					
					$shortcode = '[ajax_load_more id="quick-hit-pods" loading_style="infinite skype" container_type="div" theme_repeater="pod.php" post_type="post" category="' . $term->slug . '" scroll_distance="-600"]';
					echo do_shortcode($shortcode);
				?>
			
			</div>
		</section>			

		<?php get_template_part('template-parts/category/sidebar'); ?>

	</section>
	
<?php get_footer(); ?>