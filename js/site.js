jQuery(document).ready(function($) {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Smooth Scroll
	$('.smooth').smoothScroll();


	// Menu Toggle
	$('#toggle').click(function(){
		$('header, nav, #page').toggleClass('open');
		return false;
	});

	// Header, Nav, #page Toggle
	$('#page').on('click', function(){
		$('header, nav, #page').removeClass('open');
	});



	// Subscribe Trigger
	$('.subscribe-trigger').click(function(){
		$('body').toggleClass('subscribe-open');
		return false;
	});


	// Subscribe Close
	$('.subscribe-close').click(function(){
		$('body').toggleClass('subscribe-open');
		return false;
	});


	// Search Trigger
	$('.search-trigger').click(function(){
		$('body').toggleClass('search-open');
		return false;
	});


	// Search Close
	$('.search-close').click(function(){
		$('body').toggleClass('search-open');
		return false;
	});



	// Homepage Carousel
	$('.page-template-homepage .featured-articles-carousel').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed:  500,
		autoplay: true,
		autoplaySpeed: 6000,
		slidesToShow: 1,
		slidesToScroll: 1,
		customPaging : function(slider, i) {
		    var thumb = $(slider.$slides[i]).data('thumb');
		    return '<a><img src="'+thumb+'"></a>';
		},

	});


	// New User Popup
    var showPopup = localStorage.getItem('showPopup');
    if (showPopup == null) {
        localStorage.setItem('showPopup', 1);

        // Show popup here
        $('#new-user').fadeIn('slow');
    }

	$('.new-user-close-btn').on('click', function(){
		$('#new-user').fadeOut('slow');
		return false;
	});



	$('.combo-box select').on('change', function () {
		var url = $(this).val();
		if (url) {
			window.location = url;
		}
		return false;
	});




});

window.almComplete = function(alm){

	jQuery('.show-more-btn .btn').on('click', function(){
		var article = jQuery(this).closest('section.body');
		jQuery(article).addClass('expanded');

		return false;
	});
};


jQuery(document).keyup(function(e) {
	
	if (e.keyCode == 27) {
		jQuery('header, nav, #page').removeClass('open');
	}

});