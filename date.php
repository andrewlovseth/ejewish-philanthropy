<?php
	get_header();

	if(is_year()) {

		$label = get_the_date('Y');
		$year = get_the_date('Y');
		$alm_query = 'year="'. $year . '"';

	} elseif(is_month()) {

		$label = get_the_date('F Y');
		$month = get_the_date('n');
		$year = get_the_date('Y');
		$alm_query = 'year="'. $year . '" month="' . $month . '"';

	}

?>
    <section class="archive-page-header">
		<div class="wrapper">

			<div class="headline">
				<h1 class="x-large-title">Posts from <?php echo $label; ?></h1>
			</div>
			
		</div>
    </section>	
    	
    <main class="content grid-archive">
        <div class="wrapper">
        
            <?php get_template_part('template-parts/global/grid-archive'); ?>

        </div>
    </main>

<?php get_footer(); ?>