<?php

/*

	Template Name: About

*/

get_header(); ?>
	
	<section class="page-header">
		<div class="wrapper">

			<div class="headline">
				<h1 class="x-large-title"><?php the_title(); ?></h1>
			</div>

		</div>
	</section>


	<section class="main">
		<div class="wrapper">
		
			<section class="content copy p2">
				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</section>

			<section class="masthead">
				<div class="section-header">
					<h3>Masthead</h3>
				</div>

				<?php $posts = get_field('masthead'); if( $posts ): ?>
					<?php foreach( $posts as $p ): ?>
						<div class="position">
							<a href="<?php echo get_permalink( $p->ID ); ?>">
								<span class="name"><?php echo get_the_title( $p->ID ); ?></span>
								<span class="title"><?php the_field('title', $p->ID); ?></span>
							</a>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</section>

		</div>
	</section>


	<section class="jobs">
		<div class="wrapper">

			<div class="section-header">
				<h3><?php the_field('jobs_headline'); ?></h3>

				<div class="copy p2">
					<?php the_field('jobs_deck'); ?>
				</div>
			</div>

			<?php $posts = get_field('jobs'); if( $posts ): ?>

				<div class="job-listings">
					<?php foreach( $posts as $p ): ?>
						<article class="job">
							<div class="meta">
								<div class="category">
									<span><?php the_field('category', $p->ID); ?></span>
								</div>

								<div class="location">
									<span><?php the_field('location', $p->ID); ?></span>
								</div>
							</div>

							<div class="headline">
								<h4><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h4>
							</div>

							<div class="copy p2">
								<?php the_field('short_description', $p->ID); ?>
							</div>

							<div class="read-more">
								<a href="<?php echo get_permalink( $p->ID ); ?>">Read More</a>
							</div>

						</article>
					<?php endforeach; ?>						
				</div>

			<?php else: ?>

				<div class="job-listings no-jobs">
					<article class="job">
						<div class="headline">
							<h4>No jobs at the moment.</h4>
						</div>
					</article>
				</div>

			<?php endif; ?>			

		</div>
	</section>

<?php get_footer(); ?>