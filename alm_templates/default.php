<article class="quick-hit-preview">
	<a href="<?php the_permalink(); ?>">

		<?php if(get_field('featured_image')): ?>
			<div class="photo">
				<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		<?php endif; ?>

		<div class="info">
			<?php if(in_category('quick-hits')): ?>
				<?php if(get_field('tagline')): ?>
					<div class="tagline">
						<h4><?php the_field('tagline'); ?></h4>
					</div>
				<?php endif; ?>

			<?php elseif(in_category('daily-kickoff')): ?>
				<div class="tagline">
					<h4>Daily Kickoff</h4>
				</div>
			<?php endif; ?>

			<div class="title">
				<h3><?php the_title(); ?></h3>
			</div>

			<div class="meta">
				<em>Published: <?php the_time('l, F j, Y'); ?></em>
			</div>
		</div>
	</a>
</article>
