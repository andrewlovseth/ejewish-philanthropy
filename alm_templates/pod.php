<article <?php post_class('post cat-post'); ?> data-url="<?php the_permalink(); ?>">
	<section class="header">
		<?php get_template_part('template-parts/single-post/tagline'); ?>

		<div class="title">
			<h1 class="x-large-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		</div>
		<?php get_template_part('template-parts/single-post/dek'); ?>

		<?php get_template_part('template-parts/category/featured-image'); ?>

		<?php get_template_part('template-parts/single-post/byline'); ?>

		<?php get_template_part('template-parts/single-post/share'); ?>

		<?php get_template_part('template-parts/single-post/opinion-dek'); ?>
	</section>
	
	<section class="body p1">
		<div class="show-more-wrapper">
			<?php the_content(); ?>

			<div class="comments">
				<a href="<?php the_permalink(); ?>/#disqus_thread" class="btn comment-btn">Share your comment</a>
			</div>	
		</div>

		<div class="show-more-btn">
			<a href="#" class="subscribe-btn btn">Read More</a>
		</div>		
	</section>
</article>