<?php

/*
	Template Name: Post Archive

*/

get_header(); ?>

    <section class="archive-page-header">
		<div class="wrapper">

			<div class="headline">
				<h1 class="x-large-title">Archive</h1>
			</div>
			
		</div>
    </section>

	<section class="archive-list cat-list">
        <div class="wrapper">
            <div class="headline section-header">
                <h3>Categories</h3>
            </div>

            <div class="link-list four-col-grid grid">
                <?php 
                    $cats = get_field('categories');


                    $args = array(
                        'orderby' => 'count',
                        'order' => 'DESC',
                        'include' => $cats
                    );
                    $cats = get_categories($args);
                    foreach ( $cats as $cat ): ?>

                        <div class="link">
                            <a href="<?php echo get_category_link( $cat->term_id ); ?>">
                                <span class="underline"><?php echo $cat->name; ?> <span class="count">(<?php echo $cat->count; ?>)</span></span>
                            </a>
                        </div>

                <?php endforeach; ?>
		</div>

        </div>
			


    </section>
    
	<section class="archive-list date-list">		
        <div class="wrapper">
            <div class="headline section-header">
                <h3>Date</h3>
            </div>

            <div class="calendar">
                <?php $years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC");
                    foreach($years as $year) : ?>
                
                    <div class="year">
                        <div class="year-header">
                            <h4><a href="<?php echo get_year_link($year); ?>"><?php echo $year; ?></a></h4>
                        </div>

                        <div class="month grid four-col-grid">
                            <?php $months = $wpdb->get_col("SELECT DISTINCT MONTH(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' AND YEAR(post_date) = '".$year."' ORDER BY post_date ASC");
                                foreach($months as $month) : ?>

                                    <div class="month-link">
                                        <a href="<?php echo get_month_link($year, $month); ?>"><span><?php echo date( 'F', mktime(0, 0, 0, $month) );?></span></a>
                                    </div>

                            <?php endforeach;?>
                        </div>
                    </div>				
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    
<?php get_footer(); ?>