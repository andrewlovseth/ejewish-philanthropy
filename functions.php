<?php

require_once( plugin_dir_path( __FILE__ ) . '/functions/enqueue-styles-scripts.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/theme-support.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/disable-editor.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/acf.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/apple-news.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/register-blocks.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/osmosys-support.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/slack-preview.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/opinion-featured-image.php');
