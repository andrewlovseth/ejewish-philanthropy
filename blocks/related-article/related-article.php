<?php

/*
 * Related Article Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'related-article-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'ji-block related';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$type = get_field('type');



if($type == 'article') {
    $related_post = get_field('post');
} elseif($type == 'podcast') {
    $related_post = get_field('podcast');
}


?>

<aside id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php
        if($related_post) {
            $args = ['post' => $related_post];
            get_template_part('blocks/related-article/views/' . $type, null, $args);
        }

    ?>

</aside>