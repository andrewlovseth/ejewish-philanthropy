<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">
		
			<?php if ( have_posts() ): ?>

				<div class="posts">

					<?php while ( have_posts() ): the_post(); ?>

						<article>
							<h3><?php the_title(); ?></h3>
							<?php the_content(); ?>
						</article>

					<?php endwhile; ?>

				</div>

			<?php endif; ?>

		</div>
	</section>
	
<?php get_footer(); ?>