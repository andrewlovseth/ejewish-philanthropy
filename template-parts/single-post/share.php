<div class="share">
	<span class="label">Share</span>

	<div class="link facebook">
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" rel="external">
			<img src="<?php bloginfo('template_directory') ?>/images/facebook-icon.svg" alt="Facebook">
		</a>
	</div>

	<div class="link twitter">
		<a href="https://twitter.com/intent/tweet/?text=<?php echo get_the_title(); ?>+<?php echo get_permalink(); ?>" rel="external">
			<img src="<?php bloginfo('template_directory') ?>/images/twitter-icon.svg" alt="Twitter">
		</a>
	</div>

	<div class="link email">
		<a href="mailto:?subject=eJewish Philanthropy: <?php echo get_the_title(); ?>&body=<?php echo get_the_title(); ?>%0D%0A<?php echo get_permalink(); ?>">
			<img src="<?php bloginfo('template_directory') ?>/images/email-icon.svg" alt="Email">
		</a>
	</div>

	<?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>

</div>