<section class="body p1">
	<?php get_template_part('template-parts/single-post/dateline'); ?>		

	<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>

	<div id="disqus_thread"></div>
</section>