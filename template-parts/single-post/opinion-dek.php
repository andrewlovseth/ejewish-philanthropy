<?php if(get_field('dek')): ?>
	<?php if (has_category('opinion')): ?>

		<div class="opinion-dek">
			<h5>In Short</h5>
			<p><?php the_field('dek'); ?></p>
		</div>

	<?php endif; ?>
<?php endif; ?>