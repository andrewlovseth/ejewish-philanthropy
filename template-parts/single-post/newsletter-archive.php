<section class="newsletter-archive">

    <div class="section-header">
        <h2>Recent Your Daily Phil Newsletters</h2>
    </div>

    <div class="recent four-col-grid">					
        <?php
            $cat = get_category_by_slug('your-daily-phil');
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 8,
                'cat' => $cat->term_id,
                'order' => 'DESC'
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


            <article class="archive-post">
                <div class="timestamp">
                    <a href="<?php the_permalink(); ?>">
                        <span class="day"><?php the_time('D'); ?>, </span><span class="date"><?php the_time('M j'); ?></span>
                    </a>
                </div>

                <?php if(get_field('featured_image')): ?>

                    <div class="photo">
                        <div class="content">
                            <a href="<?php the_permalink(); ?>">
                                <img loading="lazy" src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />											
                            </a>
                        </div>
                    </div>

                <?php endif; ?>

                <div class="headline">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                </div>
            </article>

        <?php endwhile; endif; wp_reset_postdata(); ?>
    </div>

</section>