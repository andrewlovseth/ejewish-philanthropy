<?php if(!in_category('your-daily-phil') ): ?>
    <section class="quick-hits sidebar">

        <?php get_template_part('template-parts/sidebar/your-daily-phil'); ?>

        <?php get_template_part('template-parts/sidebar/news'); ?>

        <?php get_template_part('template-parts/sidebar/opinion'); ?>

    </section>
<?php endif; ?>