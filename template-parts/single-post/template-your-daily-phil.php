<section class="main your-daily-phil">

    <section class="content">
        <div class="content-wrapper">
            <?php get_template_part('template-parts/single-post/newsletter-subscribe'); ?>

            <article <?php post_class('post single-post'); ?>>

                <section class="header">
                    <?php get_template_part('template-parts/single-post/sponsored'); ?>

                    <?php get_template_part('template-parts/single-post/title'); ?>

                    <?php get_template_part('template-parts/single-post/dek'); ?>

                    <?php get_template_part('template-parts/single-post/byline'); ?>

                    <?php get_template_part('template-parts/single-post/share'); ?>
                </section>

                <?php get_template_part('template-parts/single-post/body'); ?>

            </article>

        </div>
    </section>

    <?php get_template_part('template-parts/single-post/sidebar'); ?>

    <?php get_template_part('template-parts/single-post/newsletter-archive'); ?>

</section>