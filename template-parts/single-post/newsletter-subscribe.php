<section class="newsletter-subscribe">
	<div class="info">
		<div class="info-wrapper">
			<div class="headline">
				<h3><?php $homepage = get_option('page_on_front'); the_field('subscribe_headline', $homepage ); ?></h1>
			</div>
		
			<div class="cta">
				<a href="#" class="subscribe-btn subscribe-trigger">Subscribe</a>
			</div>
		</div>
	</div>

	<div class="photo">
		<img src="<?php bloginfo('template_directory') ?>/images/newsletter-preview.png" alt="" />
	</div>
</section>


<?php
	$cat = get_category_by_slug('your-daily-phil');
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 10,
		'cat' => $cat->term_id,
		'order' => 'DESC'
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : ?>

		<div class="recent-dropdown">
			<div class="recent-dropdown-wrapper">
				<div class="recent-dropdown-outline">

					<div class="dropdown-header">
						<span>Recent Issues</span>
					</div>

					<div class="combo-box">
						<select>
							<option value="">- Select -</option>
							<?php while ( $query->have_posts() ) : $query->the_post(); ?>
								<option value="<?php the_permalink(); ?>"><?php the_time('D'); ?>, </span><span class="date"><?php the_time('M j'); ?></option>
							<?php endwhile; ?>	
						</select> 					
					</div>
					
				</div>
			</div>
		</div>

<?php endif; wp_reset_postdata(); ?>