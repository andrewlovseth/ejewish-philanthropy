<?php if (has_category('opinion')): ?>
    <div class="opinion-header">
        <h2>Opinion</h2>
    </div>
<?php endif; ?>

<?php if(get_field('tagline')): ?>
    <div class="quick-hit-date">
        <div class="tagline">
            <h4><?php the_field('tagline'); ?></h4>
        </div>
    </div>
<?php endif; ?>