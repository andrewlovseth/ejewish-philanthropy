<?php if(get_field('dek')): ?>
	<?php if (!has_category('opinion')): ?>

		<div class="dek">
			<p><?php the_field('dek'); ?></p>
		</div>

	<?php endif; ?>
<?php endif; ?>