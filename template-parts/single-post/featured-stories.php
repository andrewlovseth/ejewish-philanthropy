<section class="footer">
	<div class="footer-wrapper">
	
		<div class="headline section-header">
			<h2>Featured Stories</h2>
		</div>

		<?php
			$home = get_option('page_on_front');
			$count = 1;

			$posts = get_field('featured_articles', $home);
			$current_post = $post->ID;
			$posts_count = count($posts);

			if( $posts ): ?>

			<div class="posts">

				<?php foreach( $posts as $p ): ?>

					<?php if($current_post != $p->ID && $count <= 3): ?>

						<article>
							<a href="<?php echo get_permalink($p->ID); ?>">
								<div class="photo">
									<div class="content">
										<img loading="lazy" src="<?php $image = get_field('featured_image', $p->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>
								</div>
							
								<div class="info">
									<div class="headline">
										<?php if(get_field('tagline')): ?>
											<div class="tagline">
												<h4><?php the_field('tagline', $p->ID); ?></h4>
											</div>
										<?php endif; ?>

										<h3>
											<?php echo get_the_title($p->ID); ?>
										</h3>
									</div>
								</div>
							</a>
						</article>

					<?php endif; ?>

					<?php if($current_post != $p->ID) { $count++; } ?> 

				<?php endforeach; ?>
			
			</div>

		<?php endif; ?>

	</div>
</section>