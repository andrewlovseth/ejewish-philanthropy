<?php 

$image = get_field('featured_image');
$hide = get_field('hide_featured_image');

if($image && $hide == false): ?>

    <div class="opinion-featured-image">
        <div class="photo-wrapper">
            <?php echo wp_get_attachment_image($image['ID'], 'medium'); ?>
            <?php get_template_part('template-parts/global/photo-credit'); ?>
        </div>

        <?php if($image['caption']): ?>
            <div class="caption">
                <p><?php echo $image['caption']; ?></p>
            </div>
        <?php endif; ?>	
    </div>

<?php endif; ?>