<section class="main two-col-content">

    <section class="content">
        <div class="content-wrapper">

            <article <?php post_class('post single-post'); ?>>
                <section class="header">
                    <?php get_template_part('template-parts/single-post/sponsored'); ?>

                    <?php get_template_part('template-parts/single-post/tagline'); ?>

                    <?php get_template_part('template-parts/single-post/title'); ?>

                    <?php get_template_part('template-parts/single-post/dek'); ?>

                    <?php get_template_part('template-parts/single-post/featured-image'); ?>

                    <?php get_template_part('template-parts/single-post/byline'); ?>

                    <?php get_template_part('template-parts/single-post/share'); ?>

                    <?php get_template_part('template-parts/single-post/opinion-dek'); ?>

                </section>

                <?php get_template_part('template-parts/single-post/body'); ?>
            </article>

        </div>
    </section>

    <?php get_template_part('template-parts/single-post/sidebar'); ?>

</section>