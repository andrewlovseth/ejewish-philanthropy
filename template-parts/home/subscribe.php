<?php $home = get_option('page_on_front'); ?>

<section id="subscribe" class="subscribe<?php if(get_field('hide_mobile_preview', 'options') == true): ?> no-preview<?php endif; ?>">

	<div class="info">
		<div class="headline">
			<h1 class="x-large-title"><?php the_field('subscribe_headline', $home); ?></h1>
		</div>

		<div class="dek">
			<p><?php the_field('subscribe_dek', $home); ?></p>
		</div>

		<div class="form">
			<?php get_template_part('template-parts/global/subscribe-form'); ?>
		</div>

		<div class="pullquote">
			<blockquote>
				<p><?php the_field('subscribe_quote', $home); ?></p>
			</blockquote>

			<cite>
				<div class="photo">
					<img src="<?php $image = get_field('subscribe_quote_photo', $home); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="source">
					<h4><?php the_field('subscribe_quote_name', $home); ?></h4>
					<h5><?php the_field('subscribe_quote_details', $home); ?></h5>
				</div>
			</cite>
		</div>					
	</div>

	<?php if(get_field('hide_mobile_preview', 'options') == false): ?> 

		<div class="preview">
			<div class="frame">
				<div class="email">
					<div class="iframe-container">
						<?php if(get_field('mobile_preview', 'options')): ?>
							<iframe src="<?php the_field('mobile_preview', 'options'); ?>"></iframe>
						<?php endif; ?>
					</div>
				</div>

			</div>
		</div>

	<?php endif; ?>
</section>