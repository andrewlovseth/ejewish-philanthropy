<?php if(get_field('featured_image')): ?>
    <?php if(!has_category('opinion',$post->ID)): ?>

        <section class="featured-image">
            <div class="photo-wrapper">
                <img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php get_template_part('template-parts/global/photo-credit'); ?>
            </div>

            <?php if($image['caption']): ?>
                <div class="caption">
                    <p><?php echo $image['caption']; ?></p>
                </div>
            <?php endif; ?>	
        </section>

    <?php endif; ?>
<?php endif; ?>