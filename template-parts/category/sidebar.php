<?php
$category = get_queried_object();
?>

<section class="quick-hits sidebar">
	
	<div class="cat <?php echo $category->slug; ?> sidebar-section">
		<div class="section-header">
			<h2><?php single_cat_title(); ?></h2>
		</div>		

		<?php
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 16,
				'cat' => $category->cat_ID
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

			<?php get_template_part('template-parts/sidebar/article-teaser'); ?>

		<?php endwhile; endif; wp_reset_postdata(); ?>
	</div>

</section>