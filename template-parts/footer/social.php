<div class="social">
    <div class="header">
        <h5>Social</h5>
    </div>

    <div class="links">
        <div class="social-link facebook">
            <a href="<?php the_field('facebook', 'options'); ?>" rel="external">
                <img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-black.svg" alt="Facebook">
            </a>
        </div>

        <div class="social-link twitter">
            <a href="<?php the_field('twitter', 'options'); ?>" rel="external">
                <img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-black.svg" alt="Twitter">
            </a>
        </div>
    </div>
</div>