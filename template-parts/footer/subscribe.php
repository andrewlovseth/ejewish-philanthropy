<div class="subscribe">
    <div class="header">
        <h5>Subscribe</h5>
    </div>

    <div class="subscribe-form">
        <?php get_template_part('template-parts/global/subscribe-form'); ?>

    </div>
    
</div>