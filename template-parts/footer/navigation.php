<div class="navigation">
    <div class="header">
        <h5>Navigation</h5>
    </div>

    <div class="links">
        <?php if(have_rows('footer_navigation', 'options')): while(have_rows('footer_navigation', 'options')): the_row(); ?>

            <div class="link">
                <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
            </div>

        <?php endwhile; endif; ?>
    </div>
</div>