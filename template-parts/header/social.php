<div class="social">
    <div class="social-link facebook">
        <a href="<?php the_field('facebook', 'options'); ?>" rel="external">
            <?php if($theme == 'overlay'): ?>
                <img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-white.svg" alt="Facebook" />
            <?php else: ?>
                <img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-black.svg" alt="Facebook" />
            <?php endif; ?>
        </a>
    </div>

    <div class="social-link twitter">
        <a href="<?php the_field('twitter', 'options'); ?>" rel="external">
            <?php if($theme == 'overlay'): ?>
                <img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-white.svg" alt="Twitter" />
            <?php else: ?>
                <img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-black.svg" alt="Twitter" />
            <?php endif; ?>
        </a>
    </div>					
</div>