<div class="logo">
    <a class="logo-type" href="<?php echo site_url('/'); ?>">
        <?php if($theme == 'overlay'): ?>
            <img src="<?php $image = get_field('logo_white', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php else: ?>
            <img src="<?php $image = get_field('logo_color', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
    </a>
</div>