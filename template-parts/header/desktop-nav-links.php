<div class="desktop-nav-links">
    <?php if(have_rows('desktop_nav_links', 'options')): while(have_rows('desktop_nav_links', 'options')): the_row(); ?>

        <a href="<?php the_sub_field('link'); ?>"><span><?php the_sub_field('label'); ?></span></a>

    <?php endwhile; endif; ?>
</div>