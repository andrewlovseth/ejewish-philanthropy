<nav class="main-nav">
	<div class="nav-wrapper">

		<div class="info">
			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo_white', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="navigation">

				<div class="links">
					<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
 
					    <div class="link">
					        <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
					    </div>

					<?php endwhile; endif; ?>

					<div class="link subscribe-link">
						<a href="#" class="subscribe-trigger">Subscribe</a>
					</div>
				</div>
			</div>

			<div class="social">

				<div class="links">
					<div class="social-link facebook">
						<a href="<?php the_field('facebook', 'options'); ?>" rel="external">
							<img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-white.svg" alt="Facebook">
						</a>
					</div>

					<div class="social-link twitter">
						<a href="<?php the_field('twitter', 'options'); ?>" rel="external">
							<img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-white.svg" alt="Twitter">
						</a>
					</div>						
				</div>
			</div>
		
		</div>
		
	</div>
</nav>