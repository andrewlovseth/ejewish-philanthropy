<nav class="network">
    <?php if(have_rows('network', 'options')): while(have_rows('network', 'options')): the_row(); ?>
        <?php
            $image = get_sub_field('logo');
            $title = $image['title'];
            if( $image ):
        ?>

            <div class="site <?php echo $title; ?>">
                <a href="<?php the_sub_field('link'); ?>"<?php if($title !== 'ejp'): ?> rel="external"<?php endif; ?>>
                    <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                </a>
            </div>

        <?php endif; ?>

    <?php endwhile; endif; ?>
</nav>