<?php if(is_singular('post')): ?>

    <?php
        $title = get_the_title();
        $image = get_field('featured_image');
        $link = get_the_permalink();
        $date_published = get_the_date('c');
        $date_modified = get_the_modified_date('c');

        if(get_field('dek')) {
            $dek = get_field('dek');
        } else {
            $dek = wp_trim_words( get_the_content(), 40, '...' );
        }        
    ?>

    <meta name="description" content="<?php echo esc_html($dek); ?>">

    <meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo esc_html($title); ?>" />
	<meta property="og:description" content="<?php echo esc_html($dek); ?>" />
	<meta property="og:url" content="<?php echo $link; ?>" />
	<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
	<meta property="article:published_time" content="<?php echo $date_published; ?>" />
	<meta property="article:modified_time" content="<?php echo $date_modified; ?>" />
	<meta property="og:image" content="<?php echo $image['sizes']['large']; ?>" />
	<meta property="og:image:width" content="<?php echo $image['sizes']['large-width']; ?>" />
	<meta property="og:image:height" content="<?php echo $image['sizes']['large-height']; ?>" />
    
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@eJPhil">
    <meta name="twitter:title" content="<?php echo esc_html($title); ?>" />
    <meta name="twitter:description" content="<?php echo esc_html($dek); ?>" />
    <meta name="twitter:image" content="<?php echo $image['sizes']['large']; ?>" />

    <script>
        {
            "@context": "https://schema.org",
            "@type": "NewsArticle",
            "headline" : "<?php echo $title; ?>",
            "image": [
                "<?php echo $image['sizes']['thumbnail']; ?>",
                "<?php echo $image['sizes']['large']; ?>"
            ],
            "dateModified" : "<?php echo $date_modified; ?>",
            "datePublished": "<?php echo $date_published; ?>"
        }
    </script>

<?php endif; ?>



<link rel="preload" href="<?php bloginfo('template_directory'); ?>/fonts/TiemposHeadline-Regular.woff2" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php bloginfo('template_directory'); ?>/fonts/TiemposHeadline-RegularItalic.woff2" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php bloginfo('template_directory'); ?>/fonts/TiemposHeadline-Bold.woff2" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php bloginfo('template_directory'); ?>/fonts/TiemposHeadline-BoldItalic.woff2" as="font" type="font/woff2" crossorigin>