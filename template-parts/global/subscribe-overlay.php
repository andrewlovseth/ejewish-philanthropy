<section id="subscribe-form">
	<div class="overlay">
		<div class="overlay-wrapper">

			<?php $homepage = get_option('page_on_front'); ?>
		
			<a href="#" class="subscribe-close"></a>

			<div class="info">
				<div class="headline">
					<h1 class="x-large-title"><?php the_field('subscribe_headline', $homepage ); ?></h1>
				</div>

				<div class="dek">
					<p><?php the_field('subscribe_dek', $homepage ); ?></p>
				</div>

				<div class="form">
					<?php get_template_part('template-parts/global/subscribe-form'); ?>
				</div>				
			</div>

		</div>
	</div>
</section>

