<?php if ( have_posts() ): $count = 1; ?>

    <section class="posts">

        <?php while ( have_posts() ): the_post(); ?>

            <article class="post-<?php echo $count; ?>">
                <div class="photo">
                    <div class="content">
                        <a href="<?php the_permalink(); ?>">
                            <?php $image = get_field('featured_image'); if($image): ?>
                                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                            <?php else: ?>
                                <div class="no-photo"></div>
                            <?php endif; ?>
                        </a>								
                    </div>
                </div>

                <div class="info">
                    <div class="meta">
                        <span class="date"><?php the_time('F j, Y'); ?></span>
                    </div>
                    
                    <div class="headline">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                </div>
            </article>

        <?php $count++; endwhile; ?>

    </section>

    <?php
        the_posts_pagination(
            array(
                'mid_size'  => 1,
                'prev_text' => __('Prev'),
                'next_text' => __('Next'),
            )
        );
    ?>

<?php endif; ?>