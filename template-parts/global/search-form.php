<section id="search-form">
	<div class="overlay">
		<div class="overlay-wrapper">

			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		        <input type="search" class="search-query" autocomplete="off" placeholder="Type search term" name="s" />
		    </form>

			<a href="#" class="search-close"></a>

		</div>
	</div>
</section>