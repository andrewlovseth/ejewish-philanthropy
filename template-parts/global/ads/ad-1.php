<?php

$ad = get_field('ad_1', 'options');
$code = $ad['code'];

?>

<div class="ad-banner">
    <div class="ad ad-1">
        <?php echo $code; ?>        
    </div>
</div>