<div class="opinion sidebar-section">
    <div class="section-header">
		<h2><a href="<?php echo site_url('/category/opinion/'); ?>">Opinion</a></h2>
	</div>		

	<?php
		$category = get_category_by_slug('opinion');
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 6,
			'cat' => $category->cat_ID
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

		<?php get_template_part('template-parts/sidebar/article-teaser'); ?>

	<?php endwhile; endif; wp_reset_postdata(); ?>
</div>