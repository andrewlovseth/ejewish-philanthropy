<article class="teaser<?php if(in_category('sponsored')): ?> sponsored<?php endif; ?>">
    <a href="<?php the_permalink(); ?>">

        <?php if(get_field('featured_image')): ?>
            <div class="photo">
                <img loading="lazy" src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        <?php endif; ?>

        <div class="info">
            <?php if(in_category('sponsored')): ?>
                <div class="tagline">
                    <h4>Sponsored</h4>
                </div>
            <?php elseif(in_category('your-daily-phil')): ?>
                <div class="tagline">
                    <h4>Your Daily Phil</h4>
                </div>
            <?php elseif(get_field('tagline')): ?>
                <div class="tagline">
                    <h4><?php the_field('tagline'); ?></h4>
                </div>
            <?php endif; ?>

            <div class="title">
                <h3><?php the_title(); ?></h3>
            </div>
        </div>
    </a>
</article>