<div class="your-daily-phil sidebar-section">
    <div class="section-header">
		<h2><a href="<?php echo site_url('/category/your-daily-phil/'); ?>">Your Daily Phil</a></h2>
	</div>		

	<?php
		$category = get_category_by_slug('your-daily-phil');
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 1,
			'cat' => $category->cat_ID
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <article class="teaser your-daily-phil">
                <a href="<?php the_permalink(); ?>">
                    <div class="photo">
                        <img loading="lazy" src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>

                    <div class="info">
                        <div class="tagline">
                            <h4><?php the_time('F j, Y'); ?></h4>
                        </div>

                        <div class="title">
                            <h3><?php the_title(); ?></h3>
                        </div>
                    </div>
                </a>
            </article>

	<?php endwhile; endif; wp_reset_postdata(); ?>
</div>