<?php get_header(); ?>

	<?php if(in_category('your-daily-phil') ): ?>

		<?php get_template_part('template-parts/single-post/template-your-daily-phil'); ?>

	<?php else: ?>

		<?php get_template_part('template-parts/single-post/template-post'); ?>
	
	<?php endif; ?>
	
<?php get_footer(); ?>