<!DOCTYPE html>
<html>
<head>
	<?php get_template_part('template-parts/global/meta-tags'); ?>

	<meta name="google-site-verification" content="9Wf_4lxmCc-_O16llw8R8ogajK8S1OsmJQDB5Ktb3wU" />

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php the_field('head_javascript', 'options'); ?>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php the_field('body_top_javascript', 'options'); ?>
	<?php wp_body_open(); ?>

	<?php get_template_part('template-parts/header/nav'); ?>
	
	<div id="page">

		<?php get_template_part('template-parts/header/network'); ?>

		<header>
			<div class="wrapper">

				<?php get_template_part('template-parts/header/logo'); ?>

				<?php get_template_part('template-parts/header/desktop-nav-links'); ?>

				<div class="utilities">
					<?php get_template_part('template-parts/header/social'); ?>

					<div class="subscribe">
						<a href="<?php echo site_url('/#subscribe'); ?>" class="subscribe-trigger subscribe-btn">Subscribe</a>
					</div>

					<div class="search">
						<a href="#" class="search-btn search-trigger">
							<?php if($theme == 'overlay'): ?>
								<img src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" alt="Search" />
							<?php else: ?>
								<img src="<?php bloginfo('template_directory') ?>/images/search-icon-black.svg" alt="Search" />
							<?php endif; ?>
						</a>
					</div>

					<a href="#" id="toggle">
						<div class="patty"></div>
					</a>				
				</div>
			</div>
		</header>